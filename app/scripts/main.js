$(function () {
  function black_active(){
    $('.services').find('.tab_active').find('.line__active').show();
  }
  black_active();

  $('.service__tab').on('mousedown', function () {
    $('.tab_active').removeClass('tab_active');
    let sliderId = $(this).attr('data-id');
    $('.line__active').hide();
    $(this).addClass('tab_active').find('.line__active').show();
    black_active();
    showSlider(sliderId)

  })

  function showSlider(sliderId){
    $('.all_text__block').hide();
    $('.slider_second' + sliderId).show()
  }

  $('.search').on('click', function () {
    $('.sb-search').show();
    $('.sb-search-input').focus();
    $(this).hide()
  })

  $('.sb-search-input').on('focusout', function () {
    $('.sb-search').hide();
    $('.search').show()
  })

  $('.catalog').on('click', function () {
    $('.catalog_active').toggle();
  })

  $('#slider-range').slider({
    range: true,
    min: 0,
    max: 10000,
    values: [0, 5000],
    slide: function (event, ui) {
      setTimeout(updateRange(), 500)
    }
  });

  function updateRange() {
    let min = $('#slider-range').slider('values', 0);
    let max = $('#slider-range').slider('values', 1);
    if (min < 10) {
      min = 0;
    }
    $('.min').text(`от ${min}`);
    $('.max').text(`до ${max}`);
  }

  $('.slider1_show').on('click', function () {
    $(this).find('img').attr('src', 'images/tab-slide1.png');
    $('.slider2_show').find('img').attr('src', 'images/tab-slide0.png');
    $('.slider3_show').find('img').attr('src', 'images/tab-slide0.png');
    $('.slider1').show();
    $('.slider2').hide();
    $('.slider3').hide();
  })
  $('.slider2_show').on('click', function () {
    $(this).find('img').attr('src', 'images/tab-slide1.png');
    $('.slider1_show').find('img').attr('src', 'images/tab-slide0.png');
    $('.slider3_show').find('img').attr('src', 'images/tab-slide0.png');
    $('.slider2').show();
    $('.slider1').hide();
    $('.slider3').hide();
  })
  $('.slider3_show').on('click', function () {
    $(this).find('img').attr('src', 'images/tab-slide1.png');
    $('.slider2_show').find('img').attr('src', 'images/tab-slide0.png');
    $('.slider1_show').find('img').attr('src', 'images/tab-slide0.png');
    $('.slider3').show();
    $('.slider2').hide();
    $('.slider1').hide();
  })
});